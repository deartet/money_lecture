var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var spritesmith = require('gulp.spritesmith');
var gulpIf = require('gulp-if');
var svgSprite = require('gulp-svg-sprite');
var rename = require('gulp-rename');
var del = require('del');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var debug = require('gulp-debug');
var cached = require('gulp-cached');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var newer = require('gulp-newer');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
var runSequence = require('run-sequence'); // prevent dependency tasks from running at the same time
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');
var fs = require('fs');
var replace = require('gulp-replace');



// =================
// DEVELOPMENT PHASE
// =================

//  ********COMPILE SASS TO CSS********
// catching errors and error prevention for multiple plugins with gulp-plumber
//prevent errors from breaking gulp's watch method
function customPlumber(errTitle) {
    return plumber({
        errorHandler: notify.onError({
                //customizing error title
                title: errTitle || "Error Running Gulp",
                message: "Error: <%= error.message %>",
                //other sounds: Basso, Blow, Bottle, Frog, Funk, Glass, Hero, Morse, Ping, Pop, Pirr, Sosumi, Submarine, Tink
                sound: "Blow"
            })
            // switch below with the errorHandler with notify.onError
            //function(err){
            //log error in console
            // console.log(err.stack);
            //ends the current pipe, so gulp watch doesn't break
            //this.emit('end');
            //} 
    });
}

//// control gulp-sass the sass compile style
//var gulpSassOptions = {
//    outputStyle: 'expanded',
//    precision: 5
//};

gulp.task('compile-sass', function () {
    return gulp.src('dev/scss/**/*.scss')
        .pipe(customPlumber('Error Running Sass'))
        // sourcemaps should come before sass and autoprefixer to be functioned
        .pipe(sourcemaps.init())
        .pipe(sass())
        // run produced css through autoprefixer, put ahead css cuz we want output css are prefixed
        .pipe(autoprefixer({
            // Adds prefixs for IE8, IE9 and last 5 versions of all other browsers
            browsers: ['ie 8-9', 'last 5 versions'],
            // don't remove uncessary vendor prefixes
            remove: false
        }))
        // write sourcemap info into output css
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dev/css'))
        .pipe(browserSync.reload({
            stream: true // auto inject css into browser
        }));
});


// ******** BROWSERSYNC ********
gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: 'dev'
        },
        notify: false
    });
});

// browserSync and Sass are ran before it starts watching your sass file
// when files saved, run the specific tasks
gulp.task('watch', ['browserSync', 'compile-sass'], function () {
    // watch nunjucks, whenever saved, run nunjucks task
    gulp.watch('dev/scss/**/*.scss', ['compile-sass']);
    gulp.watch('dev/js/**/*.js', browserSync.reload);
    // reloads the browser when a html file is saved
    gulp.watch('dev/*.html', browserSync.reload);
});


//  ********IMG Sprites********
gulp.task('spritesIMG', function () {
    gulp.src('dev/images/sprites_img/**/*')
        .pipe(spritesmith({
            cssName: '_sprites_img.scss',
            imgName: 'sprites_img.png',
            // Modifies image path
            imgPath: '../images/sprites_img.png',
            retinaSrcFilter: 'dev/images/sprites_img/*@2x.png', //(Filter for retina images, a path to the reina images)
            retinaImgName: 'sprites_img@2x.png', //(Name for retina sprite)
            retinaImgPath: '../images/sprites_img@2x.png' //(Path for retina sprite)
        }))
        .pipe(gulpIf('*.png', gulp.dest('dev/images'))) // hold info about the sprites
        .pipe(gulpIf('*.scss', gulp.dest('dev/scss'))); // Image file
});


// ********SVG Sprites********
var svgConfig = {
    mode: {
        css: {
            sprite: 'sprite.svg',
            render: {
                css: {
                    template: './dev/images/sprites_svg.css'
                } // generate css file from sprite_svg mustachce template and loop css into svg folder 
            }
        } // generate css template file, info about each icon

    }
};

// begin sprite sequence by clean temp folder's sprite folder and ***img folder's svg sprite***, then will have no old svg sprite
// use del plugin to del
gulp.task('beginClean', function () {
    return del(['./dev/temp/sprites_svg', './dev/images/sprite-*.svg']);
});

// create SVG sprites
gulp.task('createspritessvg', ['beginClean'], function () {
    return gulp.src('./dev/images/sprites_svg/**/*.svg')
        .pipe(svgSprite(svgConfig))
        .pipe(gulp.dest('dev/temp/sprites_svg/'));
});


gulp.task('copyspritesgraphic', ['createspritessvg'], function () {
    return gulp.src('./dev/temp/sprites_svg/css/**/*.svg')
        .pipe(gulp.dest('./dev/images'));
});
// copy generate svg css and rename it and put into scss foler by using gulp rename plugin
// make copyspritessvgcss depend on createSprite
gulp.task('copyspritessvgcss', ['createspritessvg'], function () {
    return gulp.src('./dev/temp/sprites_svg/css/*.css')
        .pipe(rename('_sprites_svg.scss')) // change .css into .scss and become a scss partial
        .pipe(gulp.dest('./dev/scss/'));
});

// by the end of svg sprite sequence, we don't need temp folder anymore
gulp.task('endClean', ['copyspritesgraphic', 'copyspritessvgcss'], function () {
    return del('./dev/temp');
});


// run SVG sprites in general, create will finish first
gulp.task('spritesSVG', ['beginClean', 'createspritessvg', 'copyspritesgraphic', 'copyspritessvgcss', 'endClean']);



// ******** CHAIN ALL DEVELOPMENT TASKS TOGETHER ********
// sync method to delete an array of globs

// **** don't need this because this is for nunjucks generated htmls and compile-sass generated css ****
//gulp.task('clean:dev', function () {
//    return del.sync([
//        'dev/css',
//        'dev/*.html'
//    ]);
//});

// consolidated dev (development) phase task
gulp.task('dev', function (callback) {
    runSequence(
       ['spritesIMG', 'spritesSVG'], ['compile-sass'], ['browserSync', 'watch'], // tasks in array run simultaneously
        callback
    );
});





// =================
// OPTIMIZATION PHASE
// =================

// minify all css/js files in the php/html into prod folder and leave php in the root folder
// add js from html into gulp stream and concatenate js by using gulp-useref, it can concatenate css too
// useref php/html and its js path is and can be separated
// add js from html into gulp stream and concatenate js by using gulp-useref, it can concatenate css too
// minify js by using gulp-uglify
gulp.task('useref',function () {
    return gulp.src('dev/*.html')
        .pipe(useref())
        .pipe(cached('useref')) // make sure only one goes into uglify when having multiple htmls which both have js linked
        .pipe(debug()) // know what type of file go through pipe stream
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulpIf('*.js', rev())) // add cache busting to make sure css/js are always the newest
        .pipe(gulpIf('*.css', rev()))
        .pipe(revReplace()) // replace the assets with the revisioined file name
        .pipe(gulp.dest('prod'));
});



// ******** OPTMIZATION IMAGES (COMPRESSED FILE SIZE) ********
gulp.task('optimages', function () {
    return gulp.src('dev/images/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(newer('prod/images'))
        .pipe(imagemin({
            interlaced: true, //impression of downloading quicker for gif
            progressive: true, //impression of downloading quicker for jpg
            optimizationLevels: 5, // 48 trials that PNG files goes through compression
            multipass: true,
            SVGOPlugins: [
                {
                    'removeTitle': true
        },
                {
                    'removeUselessStrokeAndFill': false
        }
      ]
        }))
        .pipe(gulp.dest('prod/images'));
});


// ******** USE BROWSERSYNC TO CHECK PRODUCTION ********
// check prod folder once a while
gulp.task('browserSync:production', function () {
    browserSync.init({
        server: {
            baseDir: 'prod'
        }
    });
});

// ******** CHAIN ALL OPTMIZATION TASKS TOGETHER ********
// copy fonts to prod folder when production
gulp.task('copyFonts', function () {
    return gulp.src('dev/fonts/**/*')
        .pipe(gulp.dest('prod/fonts'));
});

// delete prod folder
gulp.task('clean:production', function () {
    return del.sync([
        'prod/**/*',
        // exclude image folder from glob
        '!prod/images',
        '!prod/images/**/*'
    ]);
});

// chain all gulp task in optimization phase and development phase, without watch and browserSync cuz no need
// use before deploy, in book the task is called "build"
gulp.task('production', function (callback) {
    runSequence(
        'clean:production', 
        ['spritesSVG', 'spritesIMG'], 
        ['compile-sass'], 
        ['useref', 'optimages', 'copyFonts'],
        callback
    );
});


// =================
// DEPLOYMENT PHASE
// =================

// ******** USE FTP METHOD TO DEPLOY (FTP SOLVE SSH PROBLEM) ********
//var creds = JSON.parse(fs.readFileSync('./secrets.json'));
//var conn = ftp.create({
//    // replace sensitive info with variables "creds"
//    host: creds.server,
//    user: creds.username,
//    password: creds.password,
//    // a log of what has happend and show in command line
//    log: gutil.log
//});
//
//gulp.task('ftp-clean', function () {
//    conn.rmdir('public_html/banner-01', function (err) {
//        if (err) {
//            console.log(err);
//        }
//    });
//});
//
//gulp.task('ftp', function () {
//    return gulp.src('prod/**/*')
//        .pipe(conn.dest('public_html/banner-01'));
//});



//// compile boostrap source scss file into dev/css
//gulp.task('compile-boostrap', function () {
//    return gulp.src('dev/scss/bootstrap.scss') // _boostrap can't compile! must have not underscore!
//        .pipe(sass())
//        .pipe(gulpIf('*.css', cssnano()))
//        .pipe(gulp.dest('dev/css'));
//});
