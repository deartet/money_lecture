$(document).ready(function () {

    // hamburger menu
    $(".burger-trigger").click(
        function(e) {
            e.preventDefault(); //點擊scroll不會回到頁首
          $(".burger-trigger").toggleClass("active");
          $(".nav-wrap").toggleClass("active");

        }
      );

        // nav menu list show and hide
      $(".nav-link").click(
        function() {
            $(".nav-link").removeClass("active");
          $(this).addClass("active");
          $(".burger-trigger").removeClass("active");
          $(".nav-wrap").removeClass("active");
        }
      );
  

        // Add smooth scrolling to all links
        $("a").on('click', function(event) {
            
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
              // Prevent default anchor click behavior
              event.preventDefault();
        
              // Store hash
              var hash = this.hash;
        
              // Using jQuery's animate() method to add smooth page scroll
              // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
              $('html, body').animate({
                scrollTop: $(hash).offset().top
              }, 600, function(){
           
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
              });
            } // End if
          });


     //限制字數
     $(".experience").each(function(){
        var maxwidth=50;
        if($(this).text().length>maxwidth){
        $(this).text($(this).text().trim().substring(0,maxwidth));
        $(this).html($(this).html()+'...');
        }
    });


   
    $(window).bind('scroll', function() {
        if ($(window).scrollTop() > 300) {
            $('.now-btn').show();
        }
        else {
            $('.now-btn').hide();
        }
   });
    
   //initiate scroll animation
   new WOW().init();


     // Disable scroll when focused on a number input.
     $('form').on('focus', 'input[type=number]', function(e) {
        $(this).on('wheel', function(e) {
            e.preventDefault();
        });
    });
 
    // Restore scroll on number inputs.
    $('form').on('blur', 'input[type=number]', function(e) {
        $(this).off('wheel');
    });
 
    // Disable up and down keys.
    $('form').on('keydown', 'input[type=number]', function(e) {
        if ( e.which == 38 || e.which == 40 )
            e.preventDefault();
    }); 

});







 
   


